import numpy as np
from scipy.linalg import sqrtm as sqrtm

class Filter:
    def __init__(self,Fmodel,Obs,finf=1.0,amp_err_model=0.,assim_method='perobs'):
        self.H=Obs.H
        self.R=Obs.R
        self.finf=finf
        self.integ=Fmodel.integ
        self.amp_err_model=amp_err_model
        self.assim_method=eval('self.'+assim_method)
        self.Fmodel=Fmodel

    def assimilation(self,X0,yt):
        nx,nens=X0.shape
        ny,ncy=yt.shape

        Xa_t=np.zeros((nx,nens,ncy))
        Xa_t[:,:,0]=X0

        Xf_t=np.zeros((nx,nens,ncy))
        Xf_t[:,:,0]=0.

        for icy in range (1,ncy):
            print('ciclo ',icy, 'de ', ncy)
            #forecast
            X0=self.integ(X0)
            Xf_t[:,:,icy]=np.copy(X0)

            #error model
            # wrk=np.random.normal(0.,1.,[nvar,nens])
            # Q=self.amp_err_model*np.einsum('ij,j...->i...',np.eye(nvar),X0)
            # X0[0:nvar]+=Q*wrk

            #add inflation
            X0_mean=np.average(X0,axis=1)[:,np.newaxis]
            X0=X0_mean+self.finf*(X0-X0_mean)
            # xf0=xf0+inf*(xf0_mean-xf0)

            X0=self.assim_method(X0,yt[:,icy],self.R[:,:,icy])

            Xa_t[:,:,icy]=np.copy(X0)

        return Xa_t,Xf_t

#-------------------------------------------------------------------------------
    def perobs(self,X,y,R):
        '''
        classical enkf, perturbed observations
        '''

        ny=y.shape[0]
        nvar,nens=X.shape
        Pf=np.cov(X)
        #compute Kalman gain
        PHt=np.einsum('ij,jk->ik',Pf,self.H.T)           #compute Pf*Ht   (Ht means transpose of H)
        HPHt=np.einsum('ij,jk->ik',self.H,PHt)           #compute H*Pf*Ht
        HPHtR=HPHt+R                               #compute H*Pf*Ht+R
        invHPHtR=np.linalg.pinv(HPHtR)              #compute the inverse of H*Pf*Ht+R
        # invHPHtR=np.transpose(invHPHtR,(0,2,1))
        K=np.einsum('ij,jk->ik',PHt,invHPHtR)             #Kalman gain
        #compute analysis
        Xobs=np.einsum('ij,jk->ik',self.H,X)     #forecast in the observation space
        wrk=np.random.normal(0, 1, [ny,nens])
        yper=np.einsum('ij,j...',R,wrk).T   #perturbed observations
        yper+=y[:,np.newaxis]
        Xgain=np.einsum('ij,jk->ik',K,yper-Xobs) #innovation
        Xa=X+Xgain

        return Xa

#-------------------------------------------------------------------------------
    def etkf(self,X,y,R):
        '''
        deterministic ensamble Kalman Filter (Ensamble transform Kalman Filter)
        '''

        ny=y.shape[0]
        nvar,nens=X.shape
        sqrens=np.sqrt(nens-1)
        #compute forecast mean
        Xmean=np.average(X,axis=1)  #analysis mean
        #compute ensamble anomalies
        X_anom=(X-np.einsum('i,j',Xmean,np.ones(nens)))/sqrens  #ensamble anomalies
        #compute observation anomalies
        Xobs=np.einsum('ij,jk->ik',self.H,X)    #analysis in the observation space
        Xobs_mean=np.einsum('ij,j->i',self.H,Xmean) # mean of the analysis in the observation space
        Y_anom=(Xobs-np.einsum('i,j',Xobs_mean,np.ones(nens)))/sqrens   #observation anomalies
        #compute ensamble transform matrix
        Rinv=np.linalg.pinv(R)  #inverse of the observation covariance matrix
        Y_anomT=Y_anom.T
        aux_Omega=Y_anomT@Rinv@Y_anom #ensamble transform matrix
        Omega=np.linalg.pinv(np.eye(nens)+aux_Omega)

        #Compute analysis in the ensamble space
        aux_wa=Omega@Y_anomT@Rinv #auxiliar calculation, compute omega@YaT@Rinv
        wa=np.einsum('ij,j->i',aux_wa,y-Xobs_mean)    #analysis in the ensamble space

        #Generate posterior ensamble
        Xa=np.einsum('i,j',Xmean,np.ones(nens))+X_anom@(np.einsum('i,j',wa,np.ones(nens))+sqrens*sqrtm(Omega))

        return Xa
