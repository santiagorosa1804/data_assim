import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import model
import obs
import filter
import time

#-------------------------------------------------------------------------------
Ntime=1000
nens=500

finf=1.05
nint=50

amp_per=.35
cov_error=0.05

dh=0.01

xt=np.zeros((6,Ntime))

#initialize true model
M=model.Model(dh=dh, nint=nint,nens=nens)
X0,Xt=M.initialization()
#initialize observations
obs=obs.Obs(Model=M,ntime=Ntime,nobs=3,obs_perturbation_amp=amp_per,cov_error=cov_error)
#make true series
Xt_t=obs.True_series(Xt)
#make observations
y_t=obs.Obs_series(Xt_t)

# #trajectory plot
# fig = plt.figure()
# # syntax for 3-D projection
# ax = plt.axes(projection ='3d')
# ax.plot(y_t[0],y_t[1],y_t[2])
# ax.plot(Xt_t[0],Xt_t[1],Xt_t[2])
#
# plt.show()
# quit()

#initialize filter
sigma=11.;rho=25.;beta=10./3.
par=[sigma,rho,beta]
parS=[.05,0.,0.]
#initialize forward model
Fmodel=model.Model(dh=dh, nint=nint,nens=nens, par=par, parS=parS)
Xf0,_=Fmodel.initialization()
F=filter.Filter(Fmodel,obs,finf=finf,amp_err_model=0.,assim_method='perobs')

Xa_t,_=F.assimilation(Xf0,y_t)
#
# quit()

#--------------------------------------------------------------------------------

scatter_array=np.linspace(0,Ntime,Ntime)

plot=True
if plot:
    fig=plt.figure(figsize=(12,8))
    ax1=fig.add_subplot(2,3,1)
    ax1.plot(Xt_t[0],label='true',zorder=1)
    ax1.scatter(scatter_array,y_t[0],label='obs',color='tab:red',s=3.,zorder=1)
    ax1.plot(np.average(Xa_t[0],axis=0),label='analysis',zorder=2)
    ax1.plot(Xa_t[0,0],label='ens',color='gray',zorder=0)
    for i in range (nens):
        ax1.plot(Xa_t[0,i],color='gray',zorder=0)
    ax1.legend()

    ax2=fig.add_subplot(2,3,2)
    ax2.plot(Xt_t[1],label='true',zorder=1)
    ax2.scatter(scatter_array,y_t[1],label='obs',color='tab:red',s=3.,zorder=1)
    ax2.plot(np.average(Xa_t[1],axis=0),label='analysis',zorder=2)
    ax2.plot(Xa_t[1,0],label='ens',color='gray',zorder=0)
    for i in range (nens):
        ax2.plot(Xa_t[1,i],color='gray',zorder=0)
    ax2.legend()
    #
    ax3=fig.add_subplot(2,3,3)
    ax3.plot(Xt_t[2],label='true',zorder=1)
    ax3.scatter(scatter_array,y_t[2],label='obs',color='tab:red',s=3.,zorder=1)
    ax3.plot(np.average(Xa_t[2],axis=0),label='analysis',zorder=2)
    ax3.plot(Xa_t[2,0],label='ens',color='gray',zorder=0)
    for i in range (nens):
        ax3.plot(Xa_t[2,i],color='gray',zorder=0)
    ax3.legend()

    ax4=fig.add_subplot(2,3,4)
    # ax4.plot(Xp_t[0],label='true',zorder=1)
    ax4.plot(np.average(Xa_t[3],axis=0),label='analysis',zorder=2)
    ax4.plot(Xa_t[3,0],label='ens',color='gray',zorder=0)
    for i in range (nens):
        ax4.plot(Xa_t[3,i],color='gray',zorder=0)
    ax4.legend()

    ax5=fig.add_subplot(2,3,5)
    # ax5.plot(Xp_t[1],label='true',zorder=1)
    ax5.plot(np.average(Xa_t[4],axis=0),label='analysis',zorder=2)
    ax5.plot(Xa_t[4,0],label='ens',color='gray',zorder=0)
    for i in range (nens):
        ax5.plot(Xa_t[4,i],color='gray',zorder=0)
    ax5.legend()

    ax6=fig.add_subplot(2,3,6)
    # ax6.plot(Xp_t[2],label='true',zorder=1)
    ax6.plot(np.average(Xa_t[5],axis=0),label='analysis',zorder=2)
    ax6.plot(Xa_t[5,0],label='ens',color='gray',zorder=0)
    for i in range (nens):
        ax6.plot(Xa_t[5,i],color='gray',zorder=0)
    ax6.legend()

    fig.tight_layout()

    fig.savefig('enkf_lorenz',dpi=300)

# plt.clf()
# plt.plot(xf[4])
# plt.show()
