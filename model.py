import numpy as np

class Model:
    def __init__(self,
                par=[10.,28.,8./3.],parS=[0.,0.,0.],
                dh=.001, nint=50, nens=100, seed=50):

        self.par=np.array([par[0],par[1],par[2]])     # initial parameters. par=[sigma,beta,rho]
        self.parS=parS                                # initial parameters dispersion
        self.dh=dh   #rk4 integration step
        self.nint=nint #number of integrations
        self.nens=nens  #number of ensambles

        self.seed=seed  #random seed

        self.nvar=3  #number of model variables
        self.npar=3  #number of model parameters


#-------------------------------------------------------------------------------
    def lorenz63(self,x0):

        #x0: input of the lorenz 63: the components are x, y, z, sigma, rho and beta (in that order)
        #x1: output, the components are the derivatives dx, dy, dz and the parameters sigma, rho and beta (in that order)

        x,y,z=x0[0:3]
        sigma,rho,beta=x0[3:6]

        dx=sigma*(y-x)
        dy=x*(rho-z)-y
        dz=x*y-beta*z

        x1=np.array([dx,dy,dz,sigma,rho,beta])

        return x1

#-------------------------------------------------------------------------------
    def integ(self,x):
        '''
        runge kutta 4 for the lorenz 63
        inputs:
            vec: vector (output of the lorenz63 model above)
            nint: number of integrations before step
        output:
            vec1: integrated model
        '''

        dh=self.dh
        dh_arr=np.array([dh,dh,dh,0,0,0])
        dh2=.5*dh
        dh2_arr=np.array([dh2,dh2,dh2,0,0,0])
        dh3=dh/3.
        dh3_arr=np.array([dh3,dh3,dh3,0,0,0])
        dh6=dh/6.
        dh6_arr=np.array([dh6,dh6,dh6,0,0,0])

        if len(x.shape)>1:
            dh_arr=dh_arr[:,np.newaxis]
            dh2_arr=dh2_arr[:,np.newaxis]
            dh3_arr=dh3_arr[:,np.newaxis]
            dh6_arr=dh6_arr[:,np.newaxis]

        for i in range (self.nint):
            k1=self.lorenz63(x)
            k2=self.lorenz63(x+k1*dh2_arr)
            k3=self.lorenz63(x+k2*dh2_arr)
            k4=self.lorenz63(x+k3*dh_arr)

            x1=x+dh6_arr*k1+dh3_arr*k2+dh3_arr*k3+dh6_arr*k4

        return x1

#-------------------------------------------------------------------------------

    def initialization(self):
        " Initializes the ensemble and the true "

        print ( 'Integrating long climatology' )
        Xt =  np.r_[ 16.42050074, 19.85134323, 34.58086525, self.par[0], self.par[1], self.par[2]]
        nt=5000
        nx=self.nvar+self.npar #full dimension of the state vector
        X_t=np.zeros((nx,nt+1))
        X_t[:,0]=Xt
        for it in range(nt):
            X_t[:,it+1]=self.integ(X_t[:,it])
        #Take initial ensemble (and true state) from climatology
        np.random.seed(self.seed) #to choose always the same xt (after clim)
        idx = np.random.choice(nt,self.nens+1,replace=True)
        Xt = X_t[:,idx[self.nens]]
        X0 = X_t[:,idx[:self.nens]]

        #perturb parameters
        wrk=np.array(self.parS)[:,np.newaxis]*np.random.randn(self.npar,self.nens)
        X0[3:6]+=wrk
        return X0,Xt
