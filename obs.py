import numpy as np

class Obs:
    def __init__(self,Model,ntime,nobs=3,obs_perturbation_amp=0.25,cov_error=.05,varobs=[0,1,2]):

        #full observation operator
        H0=[1.,0.,0.,0.,0.,0.]
        H1=[0.,1.,0.,0.,0.,0.]
        H2=[0.,0.,1.,0.,0.,0.]
        #construct H given the observed variables
        H=[]
        for varo in varobs:
            if varo==0:
                H.append(H0)
            if varo==1:
                H.append(H1)
            if varo==2:
                H.append(H2)
        self.H=np.array(H)

        self.nvar=3
        self.npar=3

        self.ntime=ntime
        self.nobs=nobs
        self.cov_error=cov_error
        self.Model=Model
        self.R=np.zeros((nobs,nobs,ntime))
        self.amp_per=obs_perturbation_amp   #amplitude of the normal perturbations

    def True_series(self,Xt):
        Tmdl=self.Model
        Xt_t=np.zeros((self.nvar+self.npar,self.ntime))
        Xt_t[:,0]=np.copy(Xt)
        for i in range(1,self.ntime):
            Xt=Tmdl.integ(Xt)
            Xt_t[:,i]=np.copy(Xt)

        return Xt_t

    def Obs_series(self,Xt_t):
        y_t=np.einsum('ij,jk->ik',self.H,Xt_t)
        y_t+=self.amp_per*np.random.normal(0.,1.,[self.nobs,self.ntime])

        #obs covariance matrix error (fraction of the observations)
        R=np.zeros((self.nobs,self.nobs,self.ntime))
        for it in range(self.ntime):
            # for iobs in range (self.nobs):
            #     R[iobs,iobs,it]=self.cov_error*np.abs(y_t[iobs,it])
            R[:,:,it]=self.cov_error*np.eye(self.nobs)
            # print(R[:,:,it])
        self.R=R
        return y_t
